/**
 *
 */
package org.training.core.job;

import de.hybris.platform.category.impl.DefaultCategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.tx.Transaction;

import java.util.List;

import org.apache.log4j.Logger;
import org.training.core.model.AssociateProductToCategoryCronJobModel;
import org.training.core.service.CustomProductService;


/**
 * @author iqranoor
 *
 */
public class AssociateProductToCategoryJob extends AbstractJobPerformable<AssociateProductToCategoryCronJobModel>
{
	private CustomProductService customProductService; // injected
	private DefaultCategoryService categoryService; // injected
	private ModelService modelService; //injected

	private final static Logger LOG = Logger.getLogger(AssociateProductToCategoryJob.class.getName());


	/**
	 * @return the customProductService
	 */
	public CustomProductService getCustomProductService()
	{
		return customProductService;
	}


	/**
	 * @param customProductService
	 *           the customProductService to set
	 */
	public void setCustomProductService(final CustomProductService customProductService)
	{
		this.customProductService = customProductService;
	}


	/**
	 * @return the categoryService
	 */
	public DefaultCategoryService getCategoryService()
	{
		return categoryService;
	}


	/**
	 * @param categoryService
	 *           the categoryService to set
	 */
	public void setCategoryService(final DefaultCategoryService categoryService)
	{
		this.categoryService = categoryService;
	}


	/**
	 * @return the modelService
	 */
	public ModelService getModelService()
	{
		return modelService;
	}


	/**
	 * @param modelService
	 *           the modelService to set
	 */
	@Override
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}


	@Override
	public PerformResult perform(final AssociateProductToCategoryCronJobModel arg0)
	{
		//logic behind the cron job
		final int xHours = arg0.getThresholdTime();
		final String categoryCode = arg0.getCategoryCode();

		// calling the CustomProjectService
		final List<ProductModel> newProducts = customProductService.getAllNewProductsOrOnesCreatedWithinXhours(xHours);

		// fetching Category on the basis of categoryCode
		final CategoryModel category = categoryService.getCategoryForCode(categoryCode);
		LOG.info("Number of new Products:" + newProducts.size());

		if (newProducts.size() > 0)
		{
			// save products in category with code categoryCode
			// CategoryModel -> setProducts()

			category.setProducts(newProducts);

			// Using ModelService to save

			Transaction tx = null;
			try
			{
				tx = Transaction.current();
				tx.begin();
				modelService.save(category);
				tx.commit();
			}
			catch (final Exception e)
			{
				if (tx != null)
				{
					tx.rollback();
				}
				LOG.error("Could not save products" + e);
			}
		}

		return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);

	}

}
