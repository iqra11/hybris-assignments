/**
 *
 */
package org.training.core.service.impl;

import de.hybris.platform.core.model.product.ProductModel;

import java.util.List;

import org.training.core.dao.CustomProductDAO;
import org.training.core.service.CustomProductService;


/**
 * @author iqranoor
 *
 */
public class CustomProductServiceImpl implements CustomProductService
{
	private CustomProductDAO customProductDAO; // injected

	@Override
	public List<ProductModel> getAllNewProductsOrOnesCreatedWithinXhours(final Integer X)
	{
		return getCustomProductDAO().findAllProductsCreatedWithinXhoursOrAreNew(X);
	}

	/**
	 * @return the customProductDAO
	 */
	public CustomProductDAO getCustomProductDAO()
	{
		return customProductDAO;
	}

	/**
	 * @param customProductDAO
	 *           the customProductDAO to set
	 */
	public void setCustomProductDAO(final CustomProductDAO customProductDAO)
	{
		this.customProductDAO = customProductDAO;
	}



}
