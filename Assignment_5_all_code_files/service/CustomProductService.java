/**
 *
 */
package org.training.core.service;

import de.hybris.platform.core.model.product.ProductModel;

import java.util.List;


/**
 * @author iqranoor
 *
 */
public interface CustomProductService
{
	public List<ProductModel> getAllNewProductsOrOnesCreatedWithinXhours(final Integer X);
}
