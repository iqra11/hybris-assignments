/**
 *
 */
package org.training.core.dao.impl;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.training.core.dao.CustomProductDAO;


/**
 * @author iqranoor
 *
 */
public class CustomProductDAOImpl implements CustomProductDAO
{
	private FlexibleSearchService flexibleSearchService; //injected

	@Override
	public List<ProductModel> findAllProductsCreatedWithinXhoursOrAreNew(final Integer X)
	{
		final StringBuilder query = new StringBuilder(
				"SELECT {pk} FROM {Product as p JOIN CatalogVersion as cv ON {cv.pk}={p.catalogversion} and {cv.version} like 'Staged' AND ({new} = true OR (SYSDATE - {creationtime})*24 <= ?X)}");

		final Map<String, Object> params = new HashMap<String, Object>();
		params.put("X", X);

		final FlexibleSearchQuery flexibleSearchQuery = new FlexibleSearchQuery(query.toString());

		flexibleSearchQuery.addQueryParameters(params);
		flexibleSearchQuery.setResultClassList(Collections.singletonList(ProductModel.class));
		final SearchResult searchResult = getFlexibleSearchService().search(flexibleSearchQuery);
		return searchResult.getResult();
	}

	public FlexibleSearchService getFlexibleSearchService()
	{
		return flexibleSearchService;
	}

	public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService)
	{
		this.flexibleSearchService = flexibleSearchService;
	}
}




