/**
 *
 */
package org.training.core.dao;

import de.hybris.platform.core.model.product.ProductModel;

import java.util.List;


/**
 * @author iqranoor
 *
 */
public interface CustomProductDAO
{
	public List<ProductModel> findAllProductsCreatedWithinXhoursOrAreNew(final Integer X);
}
